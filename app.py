from flask import Flask, redirect, render_template,url_for, request, session, make_response
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from livereload import Server
# from flask_admin import Admin

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
db = SQLAlchemy(app)
app.secret_key = 'cs50'
app.config['SESSION_TYPE'] = 'filesystem'
app.debug = True

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(80), unique=False, nullable=True)
	username = db.Column(db.String(80), unique=True, nullable=False)
	pin = db.Column(db.String(5), unique=False, nullable=False)
	date_created = db.Column(db.DateTime, default=datetime.utcnow)
	def __repr__(self):
		return '<User %r>' % self.username

class Score(db.Model):
	"""docstring for ClassName"""
	id = db.Column(db.Integer, primary_key=True)
	result = db.Column(db.String(200), nullable=False)
	user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	against = db.Column(db.String(200), nullable=True)
	date_created = db.Column(db.DateTime, default=datetime.utcnow)

	def __repr__(self):
		return '<Score %r>' % self.id


class Feedback(db.Model):
	"""docstring for ClassName"""
	id = db.Column(db.Integer, primary_key=True)
	comment = db.Column(db.String(1000), nullable=False)
	user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	rating = db.Column(db.Integer, nullable=True)
	date_created = db.Column(db.DateTime, default=datetime.utcnow)

	def __repr__(self):
		return '<Feedback %r>' % self.id


@app.route('/')
def index():
	users = User.query.order_by(User.date_created).all()
	return render_template('index.html', users=users)

@app.route('/play-game', methods=['GET', 'POST'])
def playGame():
	msg = ""
	users = User.query.order_by(User.date_created).all()
	un_1 = request.cookies.get('username_1')
	un_2 = request.cookies.get('username_2')
	if request.method == 'POST':
		username = request.form['username']
		pin = request.form['pin']
		new_user = User(username=username, pin=pin)

		exists = User.query.filter_by(username=username, pin=pin).count()
		user_exists = User.query.filter_by(username=username).count()

		if exists < 1:
			if user_exists > 0:
				msg = "Username already exists, or forget your PIN, please try another username"
				return render_template('game.html', users=users, msg=msg, user={'user_1': un_1, 'user_2': un_2})
			else:
				db.session.add(new_user)
				db.session.commit()
				#session['username_1'] = username

				un_1 = request.cookies.get('username_1')
				un_2 = request.cookies.get('username_2')
				u_1 = un_1
				u_2 = un_2
				if un_1:
					if username != un_1:
						u_2 = username
					else:
						msg = "This is player one, you should choose another player."
				else:
					u_2 = ""
					u_1 = username
				resp = make_response(render_template('game.html', users=users, msg=msg, user={'user_1': u_1, 'user_2': u_2}))

				if un_1:
					if username != un_1:
						resp.set_cookie('username_2', username)
						un_2 = username
					else:
						msg = "This is player one, you should choose another player."
				else:
					resp.set_cookie('username_1', username)
					un_2 = ""
				return resp
		else:
			un_1 = request.cookies.get('username_1')
			un_2 = request.cookies.get('username_2')
			u_1 = un_1
			u_2 = un_2
			if un_1:
				if username != un_1:
					u_2 = username
				else:
					msg = "This is player one, you should choose another player."
			else:
				u_2 = ""
				u_1 = username
			resp = make_response(render_template('game.html', users=users, msg=msg, user={'user_1': u_1, 'user_2': u_2}))

			if un_1:
				if username != un_1:
					resp.set_cookie('username_2', username)
					un_2 = username
				else:
					msg = "This is user one, you should choose another one."
			else:
				resp.set_cookie('username_1', username)
				un_2 = ""
			return resp

	elif request.method == 'GET':
		#return "Hello, World";
		return render_template('game.html', users=users, msg=msg, user={'user_1': un_1, 'user_2': un_2})
		#return redirect('/play-game')


@app.route('/submit-game', methods=['GET', 'POST'])
def submitGame():
	un_1 = request.cookies.get('username_1')
	un_2 = request.cookies.get('username_2')
	won = request.cookies.get('won')
	if request.method == 'GET':
		#submit won user
		# user = User.query.filter_by(username=won).first()
		# won_user = Score(user=user.id, result='won', against=ag.id)
		# db.session.add(won_user)
		# db.session.commit()
		#finish won user submission

		#lost user submission
		if won == un_1:
			user = User.query.filter_by(username=un_2).first()
			ag = User.query.filter_by(username=un_1).first()
			lost_user = Score(user=user.id, result='lost', against=ag.id)
			db.session.add(lost_user)
			db.session.commit()

			user = User.query.filter_by(username=un_1).first()
			ag = User.query.filter_by(username=un_2).first()
			won_user = Score(user=user.id, result='won', against=ag.id)
			db.session.add(won_user)
			db.session.commit()

		elif won == un_2:
			user = User.query.filter_by(username=un_1).first()
			ag = User.query.filter_by(username=un_2).first()
			lost_user = Score(user=user.id, result='lost', against=ag.id)
			db.session.add(lost_user)
			db.session.commit()

			user = User.query.filter_by(username=un_2).first()
			ag = User.query.filter_by(username=un_1).first()
			won_user = Score(user=user.id, result='won', against=ag.id)
			db.session.add(won_user)
			db.session.commit()

		#finish lost user submission

		return redirect('/play-game')

from sqlalchemy import func
from sqlalchemy import text
@app.route('/player-rank', methods=['GET', 'POST'])
def playerRank():
	if request.method == 'GET':
		scores = Score.query.order_by(Score.date_created).all()
		#users = User.query.group_by(User.id).order_by(func.count().desc()).all()
		#users = db.session.query(Score, func.count(user.username).label('total')).join(Score).group_by(User).order_by('total DESC')


		sql = text('SELECT user.*, count(score.user) as number_of_score from user left join score on (user.id = score.user) AND (score.result = "won") group by user.id order by number_of_score desc')
		result = db.engine.execute(sql)

		return render_template('rank.html', users=result, scores=scores)



@app.route('/game-history/<u>', methods=['GET'])
def gameHistory(u):
	if request.method == 'GET':
		user = User.query.filter_by(username=u).first()
		scores = Score.query.filter_by(user=user.id).all()
		sql = text('SELECT score.*, user.username as against from score left join user on (score.against = user.id) where (score.user='+str(user.id)+')')
		result = db.engine.execute(sql)
		return render_template('history.html', user=user, scores=result)



@app.route('/feedback', methods=['GET', 'POST'])
def feedback():
	feedbacks = Feedback.query.order_by(Feedback.date_created.desc()).all()
	msg = ""
	if request.method == 'POST':
		comment = request.form['comment']
		username = request.form['username']
		rating = request.form['rating']
		pin = request.form['pin']
		exists = User.query.filter_by(username=username, pin=pin).count()
		user = User.query.filter_by(username=username, pin=pin).first()
		user_exists = User.query.filter_by(username=username).count()

		if exists < 1:
			if user_exists > 0:
				msg = "Invalid PIN."
			else:
				msg = "Username not exist, try again."
			return render_template('feedback.html', feedbacks=feedbacks, msg=msg)
		else:
			feedback = Feedback(comment=comment, user=user.id, rating=rating)
			db.session.add(feedback)
			db.session.commit()
			return redirect('/feedback')

	else:
		return render_template('feedback.html', feedbacks=feedbacks, msg=msg)





# # set optional bootswatch theme
# app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'

# admin = Admin(app, name='microblog', template_mode='bootstrap3')
# # Add administrative views here

##custom filter
def date(d):
	d =  datetime.strptime(str(d).split(".")[0],"%Y-%m-%d %H:%M:%S")
	date = d.strftime("%d %B, %Y, %-I %p")
	return date

app.add_template_filter(date)

def get_user(idx):
	user = User.query.filter_by(id=idx).first()
	return user.username

app.add_template_filter(get_user)

if __name__ == '__main__':
	server = Server(app.wsgi_app)
	server.serve()
	app.run(debug=True) 



# YOUR PROJECT TITLE
#### Video Demo:  
https://youtu.be/gNmeAqK_9ds


#### Description: 
Tic-tac-toe is a fun game that you can play any time and anywhere as long as you have a piece of paper, a pencil, and an opponent. Tic-tac-toe is a zero sum game, which means that if both players are playing their best, then neither player will win. However, if you learn how to play tic-tac-toe and master some simple strategies, then you'll be able to not only play, but to win the majority of the time. If you want to know how to play tic-tac-toe, then see Step 1 to get started.

Draw the board. First, you have to draw the board, which is made up of a 3 x 3 grid of squares. This means it has three rows of three squares. Some people play with a 4 x 4 grid, but that is for more advanced players, and we will focus on the 3 x 3 grid here.

Have the first player go first. Though traditionally, the first player goes with "X", you can allow the first player to decide whether he wants to go with "X"s or "O"s. These symbols will be placed on the table, in the attempt to have three of them in a row. If you're going first, then the best move you can make is to move into the center. This will maximize your chances of winning, since you'll be able to create a row of three "X"s or "O"s in more combinations (4) this way than if you chose a different square.

Have the second player go second. After the first player goes, then the second player should put down his symbol, which will be different from the symbol of the first player. The second player can either try to block the first player from creating a row of three, or focus on creating his or her own row of three. Ideally, the player can do both.

Keep practicing. Contrary to popular belief, tic-tac-toe isn't purely a game of chance. There are some strategies that can help you optimize your skills and to become an expert tic-tac-toe player. If you keep playing, you'll soon learn all of the tricks to making sure you win every time -- or, at least, you'll learn the tricks to make sure you never lose. Its like 0's and x's.



#### How to Play Tic Toc 50:
There are two players required to play this game. To play this game, You need to add two players, next time when you want to play this game you just need to use your username and PIN, if you forget your username or PIN then you should add a new user and PIN to play this game. If you won, you can see your result in the player ranking table and always you can see your user history.
If you forget your PIN, don't worry, you can play another username, you need username and PIN for only your game history.


#### Stack: HTML, CSS, Javascript, Flask, Python3 


### Prerequisites:

Install python3

- [python : ^3.6](https://www.python.org/downloads/)



#### Environment Installation: 

> ```bash
> python3 -m venv venv
> source venv/bin/activate
> ```

Go to project directory and install desired library & tolls

#### Project Installation: 

> ```bash
> pip3 install -r requirements.txt
> ```

#### Migrate Database only if empty: 

Go to your python3 console then run this command

> ```bash
> from app import db
> db.create_all()
> ```

Then exit your python3 console

#### Run Project: 

> ```bash
> flask run
> ```


### Browse Project:

- [Localhost:5000 : ^3.6](http://localhost:5000/)





